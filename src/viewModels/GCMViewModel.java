package viewModels;

import java.util.List;

import org.zkoss.zul.ListModelList;
import org.zkoss.bind.annotation.Command;

import models.Device;
import models.GCMService;
import models.GCMServiceImpl;

public class GCMViewModel {
	
	/**
	 * List of available devices.
	 */
	private List<Device> deviceList = new ListModelList<Device>(Device.getDevices());
	/**
	 * The string that contains the message to send.
	 */
	private String message;
	
	/**
	 * List of selected devices.
	 */
	private List<Device> selectedDeviceList = new ListModelList<Device>();
	
	private GCMService gcmService = new GCMServiceImpl();
	
	
	public List<Device> getDeviceList() {
		return deviceList;
	}
	public void setDeviceList(List<Device> deviceList) {
		this.deviceList = deviceList;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Device> getSelectedDeviceList() {
		return selectedDeviceList;
	}
	public void setSelectedDeviceList(List<Device> selectedDeviceList) {
		this.selectedDeviceList = selectedDeviceList;
	}
	
	@Command("sendMessage")
	public boolean sendMessage(){
		return gcmService.sendMessage(selectedDeviceList, this.message);
	}

}
