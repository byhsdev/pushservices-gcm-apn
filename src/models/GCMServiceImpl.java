package models;

import java.util.ArrayList;
import java.util.List;

import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;

public class GCMServiceImpl implements GCMService {
	
	// The SENDER_ID here is the "Browser Key" that was generated when I
	// created the API keys for my Google APIs project.
	// would be get by external source.
	private static final String SENDER_ID = "YOUR-BROWSER-KEY-HERE";
	
	private boolean sendAndroidMessage(final List<String> tokenRegisterAndroidList, final String userMessage){
		
		boolean success = false;
		
		// Instance of com.android.gcm.server.Sender, that does the
		// transmission of a Message to the Google Cloud Messaging service.
		Sender sender = new Sender(SENDER_ID);
		
		// This Message object will hold the data that is being transmitted
		// to the Android client devices.  The message could be a JSON object.
		Message message = new Message.Builder()
		
		// If multiple messages are sent using the same .collapseKey()
		// the android target device, if it was offline during earlier message
		// transmissions, will only receive the latest message for that key when
		// it goes back on-line.
		.collapseKey("test2") //This will be dynamic (random).
		.timeToLive(30)
		.delayWhileIdle(true)
		.addData("message", userMessage)
		.addData("user", "toni")
		.build();
		
		try {
			// use this for multicast messages.  The second parameter
			// of sender.send() will need to be an array of register ids.
			MulticastResult result = sender.send(message, tokenRegisterAndroidList, 1);
			//sender.send(message, idRegisterList, 1);
			
			if (result.getResults() != null) {
				System.out.println(result.getResults());
				success = true;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return success;
	}
	
	private boolean sendIphoneMessage(final List<String> tokenRegisterIphoneList, final String userMessage){
		
			try {
				Push.alert(userMessage, "/home/antonio/keystore.p12", "your-keystore-pass", false, tokenRegisterIphoneList);
			} catch (CommunicationException e) {
				e.printStackTrace();
			} catch (KeystoreException e) {
				e.printStackTrace();
			}
		
		
		return true;
	}

	@Override
	public boolean sendMessage(final List<Device> selectedDeviceList, final String userMessage) {
		
		// Send the message by post method with the json data attached
		
		boolean success = true;
		
		List<String> tokenRegisterAndroidList = new ArrayList<String>();
		List<String> tokenRegisterIphoneList = new ArrayList<String>();
		
		for (Device device: selectedDeviceList){
			if (device.getMySODevice() == Device.SODevice.ANDROID){
				tokenRegisterAndroidList.add(device.getTokenRegister());
			} else if (device.getMySODevice() == Device.SODevice.IPHONE){
				tokenRegisterIphoneList.add(device.getTokenRegister());
			}
		}
		
		if (!tokenRegisterAndroidList.isEmpty())
			success &= sendAndroidMessage(tokenRegisterAndroidList, userMessage);
		if (!tokenRegisterIphoneList.isEmpty())
			success &= sendIphoneMessage(tokenRegisterIphoneList, userMessage);
		
		System.out.println(success);
		
		return success;
	}

}
