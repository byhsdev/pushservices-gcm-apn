package models;

import java.util.List;

public interface GCMService {
	
	public boolean sendMessage(final List<Device> selectedDeviceList, final String userMessage);

}
