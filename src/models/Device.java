package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Device {
	
	/**
	 * Device name.
	 */
	private String name;
	
	/**
	 * Unique ID for device.
	 */
	private String idDevice;
	
	/**
	 * Token of the registration for device assigned by GCM or APN.
	 */
	private String tokenRegister;
	
	/**
	 * The S.O. of the Device: {Android, iPhone}
	 */
	public enum SODevice {
		ANDROID, IPHONE;
	}
	
	private SODevice mySODevice;
	
	public Device(String name, String idDevice, String tokenRegister, SODevice mySODevice){
		this.name = name;
		this.idDevice = idDevice;
		this.tokenRegister = tokenRegister;
		this.mySODevice = mySODevice;
	}
	
	public SODevice getMySODevice() {
		return mySODevice;
	}
	
	public void setMySODevice(SODevice mySODevice) {
		this.mySODevice = mySODevice;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdDevice() {
		return idDevice;
	}

	public void setIdDevice(String idDevice) {
		this.idDevice = idDevice;
	}

	public String getTokenRegister() {
		return tokenRegister;
	}

	public void setTokenRegistration(String tokenRegister) {
		this.tokenRegister = tokenRegister;
	}
	
	public static Collection<? extends Device> getDevices(){
		return Arrays.asList(
				new Device("Antonio", "1", "APA91bFf8v2I974wf-czvqKXnauTmBZX-adx1OsgwZkTx5-xwtY_onAQOYxkyLttuLSQD60vy21fg6uhlMnkUltUmU5RkDAWIkJ078OqoIdlds21jt0BJthjl4xj1_2f7ky412TN1aOQw7Tbq_yEtWehUPN_IzpqpQ", SODevice.ANDROID),
				new Device("Albert", "2", "APA91bGwlzNR-xv4pG1GJagOiO0TefR219_lZ-0Kh_ASiXOVer4SSBAasf7y1fJ4x3ExLGvWNAtxUnNBGkvNjrf09ql49U4P9A_MPTWW-NYRp6QdI82y_MEvle44IbeMXnUsHDzWazTqzp-IQQdCKolv2-UqRSOefQ", SODevice.ANDROID),
				new Device("Manu iPhone", "3", "E46F16A5421ED99F30396EFC34E25E4F460A0235A83688EB3C2EC83BAE50CB2C", SODevice.IPHONE)
				);
	}
	
	public static List<String> getRegisterTokenSelectDevices(Collection<? extends Device> selectedDevicesList){
		List<String> registerTokenDevices = new ArrayList<String>();
		for (Device device: selectedDevicesList){
			registerTokenDevices.add(device.tokenRegister);
		}
		return registerTokenDevices;
	}
	
	public String toString(){
		return this.name;
	}

}
